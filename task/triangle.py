def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    if a <= 0 or b <= 0 or c <= 0:
        # Check if any side length is non-positive
        return False
    if a + b > c and a + c > b and b + c > a:
        # Check the triangle inequality condition
        return True
    else:
        return False